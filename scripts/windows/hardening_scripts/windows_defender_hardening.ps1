################################################################################################################
# Windows Defender Device Guard - Exploit Guard Policies (Windows 10 Only)
# Enable ASR rules in Win10 ExploitGuard (>= 1709) to mitigate Office malspam
# Blocks Office childprocs, Office proc injection, Office win32 api calls & executable content creation
# Note these only work when Defender is your primary AV
# Sources:
# https://www.darkoperator.com/blog/2017/11/11/windows-defender-exploit-guard-asr-rules-for-office
# https://www.darkoperator.com/blog/2017/11/8/windows-defender-exploit-guard-asr-obfuscated-script-rule
# https://www.darkoperator.com/blog/2017/11/6/windows-defender-exploit-guard-asr-vbscriptjs-rule
# https://docs.microsoft.com/en-us/windows/security/threat-protection/windows-defender-exploit-guard/enable-attack-surface-reduction
# https://demo.wd.microsoft.com/Page/ASR2
# https://www.powershellgallery.com/packages/WindowsDefender_InternalEvaluationSettings/1.2/Content/WindowsDefender_InternalEvaluationSettings.ps1
# ---------------------
#%programfiles%\"Windows Defender"\MpCmdRun.exe -RestoreDefaults
#
#Enable Windows Defender sandboxing
setx /M MP_FORCE_USE_SANDBOX 1
# Update signatures
& "$env:ProgramFiles\Windows Defender\MpCmdRun.exe" -SignatureUpdate
# Enable Defender signatures for Potentially Unwanted Applications (PUA)
Set-MpPreference -PUAProtection enable
# Enable Defender periodic scanning
reg add "HKCU\SOFTWARE\Microsoft\Windows Defender" /v PassiveMode /t REG_DWORD /d 2 /f
#
# Enable Windows Defender real time monitoring
# Commented out given consumers often run third party anti-virus. You can run either.
# NOTE: We can run both. This will be useful for right now. Hopefully it doesn't kill our script!
Set-MpPreference -DisableRealtimeMonitoring $false
reg add "HKLM\SOFTWARE\Microsoft\Windows Defender\Real-Time Protection" /v DisableRealtimeMonitoring /t REG_DWORD /d 0 /f
# Clear exclusions, in case any were set. NOTE: We have seen this in nationals. This fails if it is empty, so we check first before setting it
if ((Get-MpPreference).ExclusionExtension) {Remove-MpPreference -ExclusionExtension ((Get-MpPreference).ExclusionExtension)}
if ((Get-MpPreference).ExclusionPath) {Remove-MpPreference -ExclusionPath ((Get-MpPreference).ExclusionPath)}
if ((Get-MpPreference).ExclusionProcess) {Remove-MpPreference -ExclusionProcess ((Get-MpPreference).ExclusionProcess)}

#
# Enable early launch antimalware driver for scan of boot-start drivers
# 3 is the default which allows good, unknown and 'bad but critical'. Recommend trying 1 for 'good and unknown' or 8 which is 'good only'
reg add "HKCU\SYSTEM\CurrentControlSet\Policies\EarlyLaunch" /v DriverLoadPolicy /t REG_DWORD /d 3 /f
#
# https://twitter.com/duff22b/status/1280166329660497920
# Stop some of the most common SMB based lateral movement techniques dead in their tracks
# NOTE: Applying these rules breaks Intune/SCCM!
# XXX: These rules are incredibly restrictive, blocking *tons* of stuff
Set-MpPreference -Force -AttackSurfaceReductionRules_Ids D1E49AAC-8F56-4280-B9BA-993A6D -AttackSurfaceReductionRules_Actions Enabled
#
# Block Office applications from creating child processes
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids D4F940AB-401B-4EFC-AADC-AD5F3C50688A -AttackSurfaceReductionRules_Actions Enabled
#
# Block Office applications from injecting code into other processes
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 75668C1F-73B5-4CF0-BB93-3ECF5CB7CC84 -AttackSurfaceReductionRules_Actions enable
#
# Block Win32 API calls from Office macro
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 92E97FA1-2EDF-4476-BDD6-9DD0B4DDDC7B -AttackSurfaceReductionRules_Actions enable
#
# Block Office applications from creating executable content
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 3B576869-A4EC-4529-8536-B80A7769E899 -AttackSurfaceReductionRules_Actions enable
#
# Block execution of potentially obfuscated scripts
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 5BEB7EFE-FD9A-4556-801D-275E5FFC04CC -AttackSurfaceReductionRules_Actions Enabled
#
# Block executable content from email client and webmail
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids BE9BA2D9-53EA-4CDC-84E5-9B1EEEE46550 -AttackSurfaceReductionRules_Actions Enabled
#
# Block JavaScript or VBScript from launching downloaded executable content
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids D3E037E1-3EB8-44C8-A917-57927947596D -AttackSurfaceReductionRules_Actions Enabled
#
# Block executable files from running unless they meet a prevalence, age, or trusted list criteria
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 01443614-cd74-433a-b99e-2ecdc07bfc25 -AttackSurfaceReductionRules_Actions Enabled
#
# Use advanced protection against ransomware
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids C1DB55AB-C21A-4637-BB3F-A12568109D35 -AttackSurfaceReductionRules_Actions Enabled
#
# Block Win32 API calls from Office macro
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 92E97FA1-2EDF-4476-BDD6-9DD0B4DDDC7B -AttackSurfaceReductionRules_Actions Enabled
#
# Block credential stealing from the Windows local security authority subsystem (lsass.exe)
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids 9E6C4E1F-7D60-472F-BA1A-A39EF669E4B2 -AttackSurfaceReductionRules_Actions Enabled
#
# Block untrusted and unsigned processes that run from USB
Add-MpPreference -Force -AttackSurfaceReductionRules_Ids B2B3F03D-6A65-4F7B-A9C7-1C7EF74A9BA4 -AttackSurfaceReductionRules_Actions Enabled
#
# Enable Controlled Folder
Set-MpPreference -Force -EnableControlledFolderAccess Enabled
#
# Enable Cloud functionality of Windows Defender
Set-MpPreference -Force -MAPSReporting Advanced
Set-MpPreference -Force -SubmitSamplesConsent SendAllSamples
#
# Enable Defender exploit system-wide protection
# The below line includes CFG which can cause issues with apps like Discord & Mouse Without Borders
Set-Processmitigation -System -Enable DEP,EmulateAtlThunks,BottomUp,HighEntropy,SEHOP,SEHOPTelemetry,TerminateOnError,CFG
#
# NOTE: Do we use DC/CG?
# Enable Windows Defender Application Guard
# This setting is commented out as it enables subset of DC/CG which renders other virtualization products unsuable. Can be enabled if you don't use those
# powershell.exe Enable-WindowsOptionalFeature -online -FeatureName Windows-Defender-ApplicationGuard -norestart
#
# Enable Windows Defender Credential Guard
# This setting is commented out as it enables subset of DC/CG which renders other virtualization products unsuable. Can be enabled if you don't use those
# reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\DeviceGuard" /v EnableVirtualizationBasedSecurity /t REG_DWORD /d 1 /f
# reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\DeviceGuard" /v RequirePlatformSecurityFeatures /t REG_DWORD /d 3 /f
# reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\DeviceGuard" /v LsaCfgFlags /t REG_DWORD /d 1 /f
#
# Enable Network protection
# Enabled - Users will not be able to access malicious IP addresses and domains
# Disable (Default) - The Network protection feature will not work. Users will not be blocked from accessing malicious domains
# AuditMode - If a user visits a malicious IP address or domain, an event will be recorded in the Windows event log but the user will not be blocked from visiting the address.
Set-MpPreference -EnableNetworkProtection Enabled
#
################################################################################################################
# Enable exploit protection (EMET on Windows 10)
# Sources:
# https://www.wilderssecurity.com/threads/process-mitigation-management-tool.393096/
# https://blogs.windows.com/windowsexperience/2018/03/20/announcing-windows-server-vnext-ltsc-build-17623/
# https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/exploit-protection-reference
# https://gunnarhaslinger.github.io/Windows-Defender-Exploit-Guard-Configuration/
# https://github.com/gunnarhaslinger/Windows-Defender-Exploit-Guard-Configuration/find/master
# Windows10-v1709_ExploitGuard-DefaultSettings.xml is taken from a fresh Windows 10 v1709 Machine
# Windows10-v1803_ExploitGuard-DefaultSettings.xml is taken from a fresh Windows 10 v1803 Machine
# Windows10-v1809_ExploitGuard-DefaultSettings.xml is taken from a fresh Windows 10 v1809 Machine
# Windows10-v1903_ExploitGuard-DefaultSettings.xml is taken from a fresh Windows 10 v1903 Machine
# Windows10-v1909_ExploitGuard-DefaultSettings.xml is taken from a fresh Windows 10 v1909 Machine (but no Changes to v1903)
# Windows10-v1709_ExploitGuard-Security-Baseline.xml is taken from the official Microsoft v1709 Baseline
# Windows10-v1803_ExploitGuard-Security-Baseline.xml is taken from the official Microsoft v1803 Baseline
# Windows10-v1809_ExploitGuard-Security-Baseline.xml is taken from the official Microsoft v1809 Baseline
# Windows10-v1903_ExploitGuard-Security-Baseline.xml is taken from the official Microsoft v1903 Baseline
# Windows10-v1909_ExploitGuard-Security-Baseline.xml is taken from the official Microsoft v1909 Baseline
# ---------------------
Invoke-WebRequest -Uri https://demo.wd.microsoft.com/Content/ProcessMitigation.xml -OutFile ProcessMitigation.xml
Set-ProcessMitigation -PolicyFilePath ProcessMitigation.xml
rm -Force ProcessMitigation.xml
