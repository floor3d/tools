# Changing back example (x64):
# ftype htafile=C:\Windows\SysWOW64\mshta.exe "%1" {1E460BD7-F1C3-4B2E-88BF-4E770A288AF5}%U{1E460BD7-F1C3-4B2E-88BF-4E770A288AF5} %*

# assoc .bat=txtfile :: assoc .cmd=txtfile :: assoc .chm=txtfile :: assoc .hta=txtfile :: assoc .jse=txtfile :: assoc .js=txtfile :: assoc .vbe=txtfile :: assoc .vbs=txtfile :: assoc .wsc=txtfile :: assoc .wsf=txtfile :: assoc .ws=txtfile :: assoc .wsh=txtfile :: assoc .sct=txtfile :: assoc .url=txtfile :: assoc .ps1=txtfile

# https://seclists.org/fulldisclosure/2019/Mar/27
cmd /c assoc .reg=txtfile
# https://www.trustwave.com/Resources/SpiderLabs-Blog/Firework--Leveraging-Microsoft-Workspaces-in-a-Penetration-Test/
cmd /c assoc .wcx=txtfile

# https://bohops.com/2018/08/18/abusing-the-com-registry-structure-part-2-loading-techniques-for-evasion-and-persistence/ :: assoc .msc=txtfile :: https://www.trustwave.com/Resources/SpiderLabs-Blog/Firework--Leveraging-Microsoft-Workspaces-in-a-Penetration-Test/ :: ftype wsxfile="%systemroot%\system32\notepad.exe" "%1" :: does not work use mitigation from the article above :: https://posts.specterops.io/the-tale-of-settingcontent-ms-files-f1ea253e4d39 :: Changing back: :: reg add "HKCR\SettingContent\Shell\Open\Command" /v DelegateExecute /t REG_SZ /d "{0c194cb2-2959-4d14-8964-37fd3e48c32d}" /f :: reg delete "HKCR\SettingContent\Shell\Open\Command" /v DelegateExecute /f :: reg add "HKCR\SettingContent\Shell\Open\Command" /v DelegateExecute /t REG_SZ /d "" /f :: https://rinseandrepeatanalysis.blogspot.com/2018/09/dde-downloaders-excel-abuse-and.html :: assoc .slk=txtfile :: assoc .iqy=txtfile :: assoc .prn=txtfile :: assoc .diff=txtfile :: https://posts.specterops.io/remote-code-execution-via-path-traversal-in-the-device-metadata-authoring-wizard-a0d5839fc54f

# CVE-2020-0765 impacting Remote Desktop Connection Manager (RDCMan) configuration files - MS won't fix
cmd /c assoc .rdg=txtfile
# Mitigate ClickOnce .application and .deploy files vector
# https://blog.redxorblue.com/2020/07/one-click-to-compromise-fun-with.html
cmd /c assoc .application=txtfile
cmd /c assoc .deploy=txtfile
