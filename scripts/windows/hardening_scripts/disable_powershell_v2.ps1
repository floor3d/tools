# TODO mitigate ClickOnce .appref-ms files vector
# https://www.blackhat.com/us-19/briefings/schedule/#clickonce-and-youre-in---when-appref-ms-abuse-is-operating-as-intended-15375
# reg delete "HKLM\SOFTWARE\Classes\.appref-ms" /f
#
# Workarround for CoronaBlue/SMBGhost Worm exploiting CVE-2020-0796
# https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV200005
# Active Directory Administrative Templates
# https://github.com/technion/DisableSMBCompression

#
# Disable Powershellv2
Disable-WindowsOptionalFeature -NoRestart -Online -FeatureName MicrosoftWindowsPowerShellV2
Disable-WindowsOptionalFeature -NoRestart -Online -FeatureName MicrosoftWindowsPowerShellV2Root
