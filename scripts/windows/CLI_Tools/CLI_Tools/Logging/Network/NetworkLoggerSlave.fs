﻿module Bootstrap.Logging.Network.NetworkLoggerSlave

open System
open System.IO
open System.Net.Sockets
open System.Runtime.Serialization
open Bootstrap.Logging
open ES.Fslog
open ES.Fslog.Loggers
open Bootstrap.Servers.RPC


/// Helper function to connect to the NetworkLoggerMaster. Use this to provide
/// the TcpClient for the NetworkLoggerSlave.
/// Returns an Async representing the connection process. By the time
let connectToMasterAsync (host: string) (port: int) : Async<TcpClient> =
  async {
    // create and connect the client
    let client = new TcpClient()
    try
      do! client.ConnectAsync(host, port) |> Async.AwaitTask
    with
      | :? SocketException as ex -> printfn $"Error: Could not connect to listener: {ex.Message}"
    return client
  }

/// A NetworkLoggerSlave is a fairly dumb logger; instead of doing any logging
/// locally, it sends it off to the 'NetworkLoggerMaster', which will handle
/// the logging there.
/// The connection to the master must be provided as an argument during
/// construction
type NetworkLoggerSlave(logLevel: LogLevel, conn: TcpClient, sink: UninitializedNetworkClientPacketSink) =
  inherit BaseLogger()

  // Errors can occur in the NetworkLoggerSlave. To aid debugging, we *also*
  // want to have logging here. Thus, we get a logging provider, make our
  // logging source, and add a console logger to the log provider.
  let logProvider = new LogProvider()
  let slaveLogSource = DefaultLogSource.defaultLogSource logProvider
  do logProvider.AddLogger(new ConsoleLogger(logLevel))

  let _syncRoot = new Object()

  default val Level = logLevel with get

  override this.WriteLogEvent(logEvent: LogEvent): unit =
    if this.EventLogLevelAllowed(logEvent.Level, this.Level) then
      sink.Send(LogData logEvent)
      //async {
      //  let stream = conn.GetStream()
      //  if (not stream.CanWrite) then
      //    slaveLogSource?NetCritical("NetworkLoggerSlave cannot write to its TcpClient stream")
      //    exit 10
      //  do! serializeToXML logEvent stream slaveLogSource
      //} |> Async.Start
