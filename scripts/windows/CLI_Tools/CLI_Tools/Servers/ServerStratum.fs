﻿module Bootstrap.Servers.ServerStratum

/// The "stratum" a server belongs to. Determines what role the server plays.
/// Masters will give commands to run, and Slaves run them.
type ServerStratum = 
  | Master
  | Slave