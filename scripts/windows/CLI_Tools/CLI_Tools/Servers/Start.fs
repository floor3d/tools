﻿module Bootstrap.Servers.Start

open System.DirectoryServices
open System.Runtime.InteropServices
open Bootstrap.Servers.RPC
open FSharpPlus
open FSharpPlus.Data
open Bootstrap.CLI.RunConfig
open Bootstrap.Servers.ADUtils
open Bootstrap.Servers.Master.MasterInit
open Bootstrap.Servers.Slave.SlaveInit
open ES.Fslog

/// Run the configuration according to the stratum this server belongs to.
let stratumDispatch (sink: UninitializedNetworkClientPacketSink) : Reader<RunConfig, unit> =
  monad' {
    let! {stratum = stratum} = ask
    match stratum with
    | ServerStratum.Master -> do! masterInit
    | ServerStratum.Slave -> do! slaveInit sink
  }

/// The main entry point after the RunConfig and loggers have been set up.
/// Performs the actions that are required by its Stratum.
let startServer (sink: UninitializedNetworkClientPacketSink) : Reader<RunConfig, unit> =
  monad' {
    // TODO: We should try to avoid logging things (non-verbosely) until all
    //       the loggers are set up and connected. Therefore, this should be
    //       moved into the respective dispatched functionalities
    do! stratumDispatch sink
    return ()
  }
