﻿module Bootstrap.Servers.Hardening.ScriptAggregator

open Bootstrap.CLI.RunConfig
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.Hardening.Powershell
open Bootstrap.Servers.Hardening.Binaries
open Bootstrap.Servers.Hardening.Generic
open Bootstrap.Servers.Hardening.EventLog
open ES.Fslog

/// Collects each script and its associated hosts to run on.
/// Maps script name to supported Windows releases (DC/Member Server/Workstation)
/// and the respective script to run.
let mutable Scripts: Map<string, Set<WindowsVariant> * ScriptCb> = Map.empty

/// Register a powershell hardening script. Names are unique.
let RegisterPwshScript (name: string) (cb: ScriptCb) (variants: list<WindowsVariant>) =
  let variantSet = Set.ofList variants
  let script = variantSet, cb
  Scripts <- Map.add name script Scripts 

/// Unregister a powershell hardening script (by name), for whatever reason.
let UnregisterPwshScript (name: string) =
  Scripts <- Map.remove name Scripts
  
/// Initialize the scripts to be used by RunScripts. This must be called first
/// in order to construct the scripts. RunScripts should be called promptly after
/// CreateScriptCollection because of a ToC2ToU vuln that occurs due to the
/// script for hardening event logs being placed 
let CreateScriptCollection(): unit =
  [ RegisterBinaryAndDriverHardening() // Signed drivers, etc.
    RegisterEventlogAny()              // Yamato Security eventlog settings, for Hayabusa
    RegisterEventlogMaster()           // ^
    //RegisterGenericHardening()         // Tons of stuff that is Very Important
    RegisterUpdateDefenderSigs()
    RegisterGenericHardeningP1()
    RegisterGenericHardeningP2()
    RegisterProtectSelfFromDefender()
    RegisterGenericHardeningP3()
    RegisterGenericHardeningP4()
    RegisterGenericHardeningP5()
    RegisterGenericHardeningP6()
    RegisterGenericHardeningP7()
    RegisterGenericHardeningP8()
  ]
  |> Seq.iter (fun (name, cb, variants) -> RegisterPwshScript name cb variants)
  
/// Run all the registered scripts that apply to this machine.
let RunScripts (config: RunConfig): unit =
  let windowsVariant = GetWindowsType()
  let {log = log} = config
  Scripts
  |> Map.filter (fun _name (rels, _) -> rels.Contains(windowsVariant))
  |> Map.iter (fun name (_rels, cb) ->
    // scripts could be run in parallel, but they should be fast enough.
    // Plus, they may have order-related overrides, and parallelism will make
    // the order nondeterministic
    log?SlaveInfo $"Running hardening script: {name}"
    let res = cb()
    if not res.Success then
      P ($"Hardening script {name} failed",
         P $"Reason: {res.Error}",
         P $"Exit code: {res.ExitCode}",
         P $"Output: {res.Result}")
      |> log?SlaveErr 
    else
      log?SlaveInfo $"Hardening script {name} succeeded."
  )
  ()