﻿module Bootstrap.Servers.Hardening.ADUsers

open Bootstrap.Servers.Hardening.Powershell

/// Remove all users from the Enterprise Admins and Schema Admins group.
let removeEnterpriseAdmins(): PwshResult =
  let command = @"
  $group = Get-ADGroup 'Enterprise Admins'
  $group | Get-ADGroupMember | ForEach-Object {
    Remove-ADGroupMember -Identity $group -Members $_ -Confirm:$false
  $group = Get-ADGroup 'Schema Admins'
  $group | Get-ADGroupMember | ForEach-Object {
    Remove-ADGroupMember -Identity $group -Members $_ -Confirm:$false
  }
  "
  RunPowershellScript command FailOnOutput 5000
  
let RegisterClearDangerousGroups(): (string * ScriptCb * list<WindowsVariant>) =
  "ClearDangerousGroups-DC", removeEnterpriseAdmins, [DomainController]
