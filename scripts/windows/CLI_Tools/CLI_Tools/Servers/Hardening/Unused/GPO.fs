﻿module Bootstrap.Servers.Hardening.GPO

open Bootstrap.Servers.Hardening.Powershell
open Microsoft.GroupPolicy
open System.DirectoryServices
open System.IO

(*
let UpdateGPO (gpo: System.Guid) =
  let _gpDomain = new GPDomain()
  let gpoTarget: Gpo = _gpDomain.GetGpo(gpo)

  //Access policies under computer configuration
  let computerPolicySettings = gpoTarget.Computer.Policy

  //Access policies under user configuration
  let userPolicySettings = gpoTarget.User.Policy

  let computerRegistryPolicy = computerPolicySettings.GetRegistry(true)
  let userRegistryPolicy = userPolicySettings.GetRegistry(true)
*)

/// Enforce NoLmHash.
/// Computer Configuration\Windows Settings\Security Settings\Local Policies\Security Options
/// Network security: Do not store LAN Manager hash value on next password change
let EnforceNoLmHash (): PwshResult =
    let command = @"
        Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters' -Name 'NoLmHash' -Value 1
    "
    RunPowershellScript command FailOnOutput
    
let EnforceStrongEncryption (): PwshResult =
    let command = @"
    "
    RunPowershellScript command FailOnOutput

/// Backup all GPOs in the Domain. This is invoked as a Powershell script.
/// The provided directory is created by the function. The purpose represents both the
/// subdirectory name and the comment for the backup.
///  TODO: BackupGPO "hardening" action requires parameters. Perhaps send this as an automated RPC from the Master after initialization.
let BackupGPO (dir: string, purpose: string): PwshResult =
    let parentDir: DirectoryInfo = new DirectoryInfo(dir)
    if not (Directory.Exists(dir)) then parentDir.Create()
    let childDir = parentDir.CreateSubdirectory(purpose)
    let command = $@"
        Backup-GPO -Path {childDir.FullName} -All -Comment '{purpose}'
    "
    RunPowershellScript command FailOnOutput
    
let RegisterMiscGPOs(): (string * ScriptCb * list<WindowsVariant>) =
  "MiscGPOs-DC", EnforceNoLmHash, [DomainController]
