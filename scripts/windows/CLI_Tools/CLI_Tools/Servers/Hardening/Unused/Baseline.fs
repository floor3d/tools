﻿module Bootstrap.Servers.Hardening.Baseline

open Bootstrap.Servers.Hardening.Powershell

/// Run the script located in PWD\MicrosoftSecurityBaseline\Local_Script\BaselineLocalInstall.ps1.
/// Potential hardening actions:
/// -Win10DomainJoined      - Windows 10 v1809, domain-joined
/// -Win10NonDomainJoined   - Windows 10 v1809, non-domain-joined
/// -WS2019Member           - Windows Server 2019, domain-joined member server
/// -WS2019NonDomainJoined  - Windows Server 2019, non-domain-joined
/// -WS2019DomainController - Windows Server 2019, domain controller
let runBaselineLocalInstall(): PwshResult =
    let argument = 
      match GetWindowsType() with
      | Workstation -> "-Win10DomainJoined"
      | MemberServer -> "-WS2019Member"
      | DomainController -> "-WS2019DomainController"
    let pwd = System.IO.Directory.GetCurrentDirectory()
    let command = $@"iex '{pwd}\MicrosoftSecurityBaseline\Local_Script\BaselineLocalInstall.ps1 {argument}'"
    RunPowershellScript command FailOnOutput 

let RegisterBaseline(): (string * ScriptCb * list<WindowsVariant>) =
  "BaselineLocalInstall-Any", runBaselineLocalInstall, AnyVariant