﻿module Bootstrap.Servers.Hardening.Powershell

open System.Threading
open System.Threading.Tasks

type PwshResult = 
    { Result: string
      Success: bool
      Error: string
      ExitCode: int }

/// Powershell script callbacks.
type ScriptCb = unit -> PwshResult

let MergePwshResult (r1: PwshResult) (r2: PwshResult): PwshResult =
    { Result = $"{r1.Result}{r2.Result}"
      Success = r1.Success && r2.Success
      Error = $"{r1.Error}${r2.Error}"
      ExitCode = if r1.ExitCode = 0 then r2.ExitCode else r1.ExitCode 
    }
let MergePwshResults (ress: seq<PwshResult>): PwshResult =
     let emptyRes = { Result = ""; Success = true; Error = ""; ExitCode = 0}
     Seq.fold MergePwshResult emptyRes ress

/// Run a powershell command, disabling the execution policy and returning 
/// the output. Requires the command as a string and a predicate that determines
/// whether execution was a success via the PwshResult the script. The callback's
/// Success field is always false. 
let RunPowershellScript (command: string) (successPred: PwshResult -> bool) (timeoutMillis: int): PwshResult =
    let startInfo = new System.Diagnostics.ProcessStartInfo()
    let commandFast = $"$ProgressPreference = 'SilentlyContinue'\n{command}"
    let psCommandBytes = System.Text.Encoding.Unicode.GetBytes(commandFast)
    let psCommandBase64 = System.Convert.ToBase64String(psCommandBytes)

    startInfo.FileName <- "powershell.exe"
    startInfo.Arguments <- $"-NonInteractive -NoProfile -ExecutionPolicy Bypass -EncodedCommand {psCommandBase64}"
    startInfo.UseShellExecute <- false
    startInfo.RedirectStandardOutput <- true
    startInfo.RedirectStandardError <- true
    startInfo.CreateNoWindow <- true

    let proc = new System.Diagnostics.Process()
    proc.StartInfo <- startInfo
    proc.Start() |> ignore
    let cancel = async {
        do! Async.Sleep timeoutMillis
        printf $"Killing Hardening Script: Timed Out ({timeoutMillis}ms)"
        proc.Kill()
        printf "Hardening Script Killed."
        return Some ()
    }
    let runProc = async {
        let _ = proc.WaitForExit()
        return Some ()
        }
    Async.Choice [cancel; runProc] |> Async.RunSynchronously |> ignore
    let output = proc.StandardOutput.ReadToEnd()
    let error = proc.StandardError.ReadToEnd()
    let intermediatePwshRes =
        { Result = output; Success = false; Error = error; ExitCode = proc.ExitCode}
    let success = successPred intermediatePwshRes

    { Result = output; Success = success; Error = error; ExitCode = proc.ExitCode}
    
/// Convenience definition for a successPred. Fails when the return code is nonzero.
let FailOnNonZero (ret: PwshResult) = ret.ExitCode = 0

/// Convenience definition for a successPred. Always succeeds.
let AnyCode _ = true
let AnyResult _ = true

/// Convenience definition for a successPred. Succeeds when the specified value
/// is returned.
let FailWhenNot (exitCode: int) (ret: PwshResult) = ret.ExitCode = exitCode

/// Some powershell commands are silent unless a failure occurs. This successPred
/// fails when any output is produced.
let FailOnOutput (ret: PwshResult) = ret.Result.Length = 0 && ret.Error.Length = 0

type WindowsVariant = 
    | DomainController
    | MemberServer
    | Workstation

/// Convenience definition of all supported windows variants.
let AnyVariant: list<WindowsVariant> = [DomainController; MemberServer; Workstation]

/// Detect if this machine is running Windows Server or Windows 8/10/11.
let GetWindowsType(): WindowsVariant = 
    let command = @"
      $productType = (Get-CimInstance -ClassName Win32_OperatingSystem).ProductType
      $compName = $env:COMPUTERNAME
      switch ($productType)
      {
          1 {""$compName is a workstation.""; break}
          2 {""$compName is a domain controller.""; break}
          3 {""$compName is a server.""; break}
      }"
    let result = RunPowershellScript command AnyCode 1000
    if result.Result.Contains("workstation") then Workstation
    elif result.Result.Contains("server") then MemberServer
    else DomainController

// let AddCmdlet (ps: PowerShell) (line: string): PowerShell =
//     ps.AddStatement().AddCommand(line)
//     
// let Line (ps: PowerShell) (line: string) (args: string seq): PowerShell =
//     ps.AddStatement().AddCommand(line)