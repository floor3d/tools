﻿module Bootstrap.Servers.Master.View.Throbber

/// Interface for a basic text-based throbber (a throbber is an idle/progress animation, such
/// as the spinning circle or rainbow beachball).
type IThrobber =
  /// Get the next frame of the throbber animation
  abstract Current: unit -> string with get
  abstract EndAnim: unit -> unit
  abstract ResumeAnim: unit -> unit

type ConnectionThrobber() =
  let mutable endSeq = false
  
  let sequence = "|/-\\"
  
  // let mutable nextFrame' =
  //         Seq.initInfinite (fun index ->
  //           let modIndex = index % sequence.Length
  //           if not endSeq then
  //             sequence[modIndex].ToString()
  //           else "")
  // let nextFrame = nextFrame'.GetEnumerator()
  let mutable index = 0
    
  interface IThrobber with
    member this.Current =
      if not endSeq then
        let ret = sequence[index % sequence.Length]
        index <- index + 1
        $"[{ret.ToString()}]"
      else
        ""

    member this.EndAnim() = endSeq <- true
    member this.ResumeAnim() = endSeq <- false
