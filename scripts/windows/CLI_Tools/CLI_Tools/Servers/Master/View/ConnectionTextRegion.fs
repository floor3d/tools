﻿module Bootstrap.Servers.Master.View.ConnectionTextRegion

open System
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.Master.Model.ISlaveModel
open Bootstrap.Servers.Master.View.ITextRegion
open Bootstrap.Servers.RPC

type Posn = int * int

/// An ITextRegion that only represents a single thing happening; that is,
/// it only renders a single Model (connection to a slave).
type ConnectionTextRegion(conn: ISlaveModel, posn: Posn, debug: bool) =
  let render(logEntry) =
    if not debug
    then
      P ($"Status: {conn.ComputerName}",
         P $"Connection: {conn.ConnectionStatus}",
         P $"Recent log entry: {logEntry}")
    else
      P ($"Status: {conn.ComputerName}",
         P $"Connection: {conn.ConnectionStatus}",
         P $"Debug: Posn: %A{posn}",
         P $"Recent log entry: {logEntry}")
  // note: getting the recent log entry is a side-effectful operation, so instead
  // we just store the last rendering in order to make Width and Height coherent
  // and reflective of the returned rendered region
  let mutable lastRender = ""
  let mutable largestWidth = 0

  /// Whether to display debug related information
  member val public Debug = false with get, set

  interface ITextRegion with
    member this.Height =
      let mutable count = 0
      for char in lastRender do
        if char = '\n' then
          count <- count + 1
      count

    member this.Width =
      let mutable max = 0
      for line in lastRender.Split([|'\n'|]) do
        let len = line.Length
        if max < len then
          max <- len
      max
      
    member val Posn = posn with get, set
    
    member this.Render() =
      async {
        let ret = lastRender
        let logEntry = conn.NewestLogEntry
        lastRender <- render(logEntry).ToString()
        let len = lastRender.Length
        if largestWidth < len then largestWidth <- len
        return ret
      }

    member this.Draw() =
      async {
        let render = lastRender
        let lastPosn = Console.GetCursorPosition()
        let mutable curY = snd posn
        let startX = fst posn
        // after each line, reset the cursor to the posn X and new Y
        lock _stdoutSync (fun _ ->
          for line in render.Split([|'\n'|]) do
            let len = max line.Length largestWidth
            let clearLine = String.replicate len " "
            // overwrite the area we are drawing with spaces
            Console.SetCursorPosition(startX, curY)
            printf $"{clearLine}"
            Console.SetCursorPosition(startX, curY)
            printf $"{line}"
            curY <- curY + 1
          Console.SetCursorPosition(lastPosn.ToTuple()))
     }
