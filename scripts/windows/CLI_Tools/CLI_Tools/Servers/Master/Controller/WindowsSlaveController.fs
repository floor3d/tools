﻿module Bootstrap.Servers.Master.Controller.WindowsSlaveController

open Bootstrap.Servers.Master
open Bootstrap.Servers.Master.IRpcReceiver
open Bootstrap.Servers.Master.Model
open Bootstrap.Servers.Master.Model.ISlaveModel
open Bootstrap.Servers.Master.View.ITextRegion
open Bootstrap.CLI.RunConfig
open Bootstrap.Servers.RPC
open FSharpPlus.Data
open FSharpPlus
open ES.Fslog
open WatsonTcp

// Controller must be an IRpcReceiver to receive a callback from the RpcServer
// and LogServer

type WindowsSlaveController private (model: ISlaveModel, view: ITextRegion, rpcServer: IRpcAcceptor, env: RunConfig) =
  let mutable doneDrawing = true

  /// The inner SlaveConnection
  /// HACK: This should expose a read-only ISlaveModel-like interface
  member val Connection: ISlaveModel = model with get

  static member Create(model: ISlaveModel, view: ITextRegion, rpcServer: IRpcAcceptor): Reader<RunConfig, WindowsSlaveController> =
    monad' {
     let! env = ask
     let controller = WindowsSlaveController(model, view, rpcServer, env)
     // register with the rpc and log server
     rpcServer.RegisterController model.ComputerName controller
     // logServer.RegisterController model.ComputerName controller
     return controller
    }

  /// Convenience method to draw the method after updating the model
  member this.Draw(): unit =
    if not doneDrawing
    then () // just do nothing
    else // asynchronously update
      doneDrawing <- false
      task {
        env.log?Verbose "Drawing a single frame"

        view.Draw() |> Async.RunSynchronously // draw the older frame
        // render the next frame
        view.Render() |> Async.RunSynchronously |> ignore
        doneDrawing <- true
      } |> ignore
      ()

  interface IRpcReceiver with
    member this.ReceiveLogMessage (logRpc: LogRpc) =
      model.ReceiveLogMessage logRpc
      //view.ReceiveLogMessage logRpc

    member this.ReceiveControlMessage (controlRpc: ControlRpc) (result: string) =
      model.ReceiveControlMessage controlRpc result
      //view.ReceiveControlMessage controlRpc result
    
    member this.ReceiveHelloMessage (hello: HelloRpc) =
      model.ReceiveHelloMessage hello

    member this.Connected (connEvent: ConnectionEventArgs) =
      env.log?SlaveInfo $"{model.ComputerName} Connected"
      //view.Connected connEvent
      model.Connected connEvent

    member this.Disconnected (disconnEvent: DisconnectionEventArgs) =
      env.log?SlaveWarn $"{model.ComputerName} Disconnected"
      //view.Disconnected disconnEvent
      model.Disconnected disconnEvent
