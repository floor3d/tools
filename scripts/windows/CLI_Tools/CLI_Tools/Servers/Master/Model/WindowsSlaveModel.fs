﻿module Bootstrap.Servers.Master.Model.WindowsSlaveModel

open System
open System.IO
open System.Net
open System.Net.Sockets
open System.Threading
open Bootstrap.CLI
open Bootstrap.CLI.RunConfig
open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.Master.Model.ISlaveModel
open Bootstrap.Servers.Master.PortFactory
open Bootstrap.Servers.Master.View.Throbber
open FSharpPlus.Data
open FSharpPlus
open ES.Fslog
open WatsonTcp
open Bootstrap.Servers.RPC
open Bootstrap.Servers.Master.IRpcReceiver
open System.Diagnostics

/// <summary>
/// The RPC server/connection for a particular slave.
/// We must track the following information about each slave:
/// <para>
/// <list type="bullet">
///   <item> <description>
///     (Listen) Logging port/logging connection
///   </description> </item>
///   <item> <description>
///     (Connect) RPC port/RPC connection (note: need to know when RPC is in-flight)
///   </description> </item>
///   <item> <description>
///     Name of the computer
///   </description> </item>
///   <item> <description>
///     Who the remote process is running as
///   </description> </item>
/// </list>
/// </para>
/// <para>
/// We must be able to do the following with each slave:
/// <list type="bullet">
///   <item> <description>
///     Run a powershell command
///   </description> </item>
///   <item> <description>
///     Run a custom REPL-like query (see reMarkable notes)
///   </description> </item>
///   <item> <description>
///     List autoruns, running processes, etc. Misc remote management tasks
///   </description> </item>
///   <item> <description>
///     Harden generically (i.e. any configuration, make no assumptions)
///   </description> </item>
/// </list>
/// </para>
/// </summary>
/// TODO: Refactor this into an interface?
type WindowsSlaveModel private (Config: RunConfig, ComputerName: string) =
  let mutable InternalConnectionStatus: string ref = ref "Awaiting Client Hello"
  let mutable InternalWindowsVersion: string ref = ref "..."
  let mutable LogRpcHistory: LogRpc seq = Seq.empty

  // for a nice and pretty animation
  let connectionThrobber: IThrobber = new ConnectionThrobber()
  do
    connectionThrobber.ResumeAnim() // We want the animation to start playing immediately

  /// The last log event we received from the slave
  let mutable LastGoodLogEvent: Option<LogRpc> = None
  /// Whether the Slave is connected to the server or not
  let mutable SlaveIsConnected = false

  let _logSync = new Object()

  /// Create a WindowsSlaveModel for the specified computer. The name must be valid
  /// in the domain (i.e., it must exist and be backed by a computer), but this
  /// is not checked.
  static member Create(computerName: string, env: RunConfig): ISlaveModel =
    let conn = new WindowsSlaveModel(env, computerName)
    conn


  interface IRpcReceiver with
    member this.Connected (connEnv: ConnectionEventArgs) =
      P ("Slave Connected to server",
        P $"Slave: {ComputerName}")
      |> Config.log?SlaveVerbose
      SlaveIsConnected <- true
      InternalConnectionStatus.Value <- "Established"
      connectionThrobber.EndAnim()


    member this.Disconnected (connEnv: DisconnectionEventArgs) =
      P ("Slave Disconnected from server",
        P $"Slave: {ComputerName}",
        P $"Reason: {connEnv.Reason}")
      |> Config.log?SlaveWarn
      SlaveIsConnected <- false
      connectionThrobber.ResumeAnim()

    member this.ReceiveLogMessage (rpc: LogRpc) =
      Config.log?SlaveVerbose $"Received new log message from slave {ComputerName}"
      lock _logSync (fun _ ->
        LastGoodLogEvent <- Some rpc
        LogRpcHistory <- Seq.append LogRpcHistory [rpc]
      )

    member this.ReceiveControlMessage (rpc: ControlRpc) (result: string) =
      // Note that these are not actions to perform locally, or actions that
      // must be performed by the Slave. These are Rpc-specific messages
      // from a Slave. E.g., a Slave will respond with "Pong" after receiving
      // a `Ping`, which will be in the form of a ControlRpc

      lock _logSync (fun _ ->
        let logRpcData = RpcData(rpc, result)
        LastGoodLogEvent <- Some logRpcData
        LogRpcHistory <- Seq.append LogRpcHistory [logRpcData]
      )

      // TODO: Print the result of receiving an Rpc result to a special area on
      // the screen, like a minibuffer/dedicated buffer in Emacs.
      P ("Received response after performing Rpc.",
        P $"Rpc: {rpc}",
        P $"Result:\n{result}")
      |> Config.log?SlaveInfo
    member this.ReceiveHelloMessage (hello: HelloRpc) =
      InternalWindowsVersion.Value <- hello.windowsVersion

  interface ISlaveModel with
    member val ComputerName = ComputerName with get
    member val ConnectionStatus = InternalConnectionStatus with get
    member val WindowsVersion = InternalWindowsVersion with get
    
    // member this.WindowsVersion
    //   with get() =
    //     let foo = fun () -> InternalWindowsVersion
    //     foo()

    /// Use `psexec64` to bootstrap the remote computer. This will block for a while,
    /// so it should likely be used in an async computation expression.
    member this.BootstrapRemote(): unit =
      try
        // make sure to log that we told the client to bootstrap
        Config.log?SlaveInfo $"Initiating bootstrap sequence for {ComputerName}"
        // copy self over with File.Copy, overwriting if required
        // FIXME: figure out the name of the current executable
        File.Copy(@".\bootstrap.exe", @$"\\{ComputerName}\C$\bootstrap.exe", true)
        // psexec to execute self
        let processInfo: ProcessStartInfo  = new ProcessStartInfo()
        //processInfo.FileName <- ".\PsExec64.exe" // HACK: PSEXEC64 must be included with the bootstrap binary
        processInfo.FileName <- "PsExec64.exe" // HACK: PSEXEC64 must be included with the bootstrap binary
        // HACK: the arguments to PSEXEC include *the Masters name* rather than a programmatically specified name; indirect bootstrapping is *impossible* currently.
        // HACK: The current Master port is 42069. Perhaps we should try to find an alternative port, should this one be (unfortunately) taken?
        processInfo.Arguments <- @$"-accepteula -w C:\ -s -d \\{ComputerName} C:\bootstrap.exe --outfile out.txt --verbosity verbose --stratum slave --listener {Config.machineName} 42069"
        use process': Process = Process.Start(processInfo)
        process'.WaitForExit()
        Config.log?SlaveInfo $"PsExec for computer {ComputerName} exited with code {process'.ExitCode}"
        // if psexec fails, tell human to run manually 3:
        if process'.ExitCode <= 0 then // Note: ExitCode = remote process's PID
          P ("PsExec did not run successfully",
            P $"Slave: {ComputerName}",
            P $"Command: {processInfo.FileName} {processInfo.Arguments}",
            P $"Exit Code: {process'.ExitCode}",
            P "Resolution: Run the command manually.")
          |> Config.log?SlaveErr
        else
          Config.log?SlaveInfo $"Done bootstrapping {ComputerName}, it's on its own!"
      with
      | e -> Config.log?SlaveErr $"Failed to bootstrap {ComputerName}: {e.Message}"

    member this.NewestLogEntry: Option<LogRpc> = LastGoodLogEvent

    member this.RetrieveLogHistory(): Option<LogRpc seq> =
      if Seq.isEmpty LogRpcHistory then
        None
      else
        Some LogRpcHistory

    member this.GetStatusPretty(): PTree<string> =
      P ($"Status: {ComputerName}",
        P $"Connection: {InternalConnectionStatus} {connectionThrobber.ToString()}",
        P $"Recent log entry: {(this :> ISlaveModel).NewestLogEntry}")


    // TODO: use an IRpcAcceptor to send Rpcs
    // TODO: abstract out the Map<..., IRpcReceiver> collection in the RpcServer
    //       to allow it to be reused for the REPL
