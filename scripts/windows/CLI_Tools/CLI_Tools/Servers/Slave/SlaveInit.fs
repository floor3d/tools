﻿module Bootstrap.Servers.Slave.SlaveInit

open Bootstrap.Logging.Pretty.PTree
open Bootstrap.Servers.RPC
open Bootstrap.Servers.Hardening.ScriptAggregator
open FSharpPlus
open FSharpPlus.Data
open Bootstrap.CLI.RunConfig
open Bootstrap.Servers.Generic
open Bootstrap.Servers.Slave.RpcClient
open ES.Fslog


/// MAKE SURE TO DO THE FOLLOWING:
/// - Whitelist the IP of the master for connections; do this in the RPC client
/// - Verify every message received from the master (don't rely on TCP seq.no.)
/// - Connections:
///     - (Connect) logging connection to master
///     - (Listen) listen for the master connecting to our server and sending
///       an RPC
/// - Slaves must send back their capabilities (i.e., if they are a Workstation,
///   Domain Controller, CA, IIS Webserver, DNS, Exchange, AD FS <noun>, etc.).
///   Outside of AD DS, this can include software like Apache, MySQL, MariaDB,
///   Sqlite, etc.
let slaveInit (sink: UninitializedNetworkClientPacketSink) : Reader<RunConfig, unit> =
  monad' {
    let! env = ask
    let {log = log; stratum = stratum; machineName = machineName} = env
    do! logSystemInfo
    // TODO: Slave: Start the RpcClient, Connect to Master (Name is passed as a CLI argument)
    log?SlaveInfo $"{machineName}: Creating RPC Client"
    let rpcClient = RpcClient.Create(env)
    log?SlaveInfo $"{machineName}: Starting RPC Client"
    try
      rpcClient.Start()
    with
    | ex ->
      P ("Failed to start RpcClient",
         P $"Slave: {machineName}",
         P $"Reason: {ex.Message}",
         P $"Stack trace:\n{ex.StackTrace}")
      |> log?SlaveCritical
      exit -20

    sink.Flush rpcClient.Client
    log?SlaveVerbose $"{machineName}: Creating script collection"
    CreateScriptCollection()
    log?SlaveInfo $"{machineName}: Running hardening scripts"
    RunScripts env
    log?SlaveInfo $"{machineName}: Hardening complete"
    while true do
      System.Threading.Thread.Sleep(1000)
    // TODO: Master: Run initial hardening
    // TODO: Slave: Run initial hardening
    // TODO: Slave, Master: Store all performed changes in a log
    // TODO: Software-specific hardening.
    // TODO: Slave: Wait for and act on RPCs
    // TODO: <After> Enumerate non-installed software, such as Apache, MySQL, MariaDB, Sqlite, XAMP, etc.
    // TODO: <After> Cache queries
    // TODO: <After> Slave: Run a REPL for the master to query
    // TODO: <Stretch> Slave: Write a Minifilter driver to monitor file system changes
    // TODO: <Stretch> Slave: Run a REPL for the master to query


    ()
  }
