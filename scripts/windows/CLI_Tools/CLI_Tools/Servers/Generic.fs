﻿/// Generic utilities for all types of servers, regardless of stratum.
module Bootstrap.Servers.Generic

open System
open System.Security.Cryptography
open FSharpPlus.Data
open FSharpPlus
open Bootstrap.CLI.RunConfig
open ES.Fslog
open Bootstrap.Logging.Pretty.PTree
open System.Text


/// Caclulate the Guid given a name, returning the name and guid as a pair.
let CalculateGuid (name: String): String * Guid =
  use md5: MD5 = MD5.Create()
  let hash = md5.ComputeHash(Encoding.UTF8.GetBytes(name))
  (name, Guid(hash))

/// Log a message about the system information and RunConfig to the logger
/// in the environment.
let logSystemInfo: Reader<RunConfig, unit> =
  monad {
    let! {log = log; stratum = stratum; machineName = machineName} = ask
    P ("Starting bootstrap sequence",
       P $"Machine: {machineName}",
       P $"Stratum: {stratum}",
       P $"Operating System: {System.Environment.OSVersion}",
       P ($"Running as: {System.Environment.UserName}",
          P $"Is Privileged: {System.Environment.IsPrivilegedProcess}")
       ) |> log?Info
  }