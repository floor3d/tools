﻿module Bootstrap.CLI.Setup.AddNetworkSlave

open Argu
open Bootstrap.CLI.Arguments
open Bootstrap.CLI.Setup.Utils
open Bootstrap.Logging.Network.NetworkLoggerSlave
open Bootstrap.Servers.RPC
open ES.Fslog

/// Add the network slave to the LogProvider, if it is present in the CLI args.
/// To be used with the |> operator.
let addNetworkSlave
  (
    results: ParseResults<Arguments>,
    logProvider: LogProvider,
    logSource: LogSource,
    sink: UninitializedNetworkClientPacketSink
  ) : ParseResults<Arguments> * LogProvider * LogSource =
  let listener = results.TryGetResult Listener
  let logLevel = getLogLevel results
  // If the listener isn't None, make a NetworkLoggerSlave and add it to the
  // LogProvider
  listener
  |> Option.map (fun (host, port) ->
    let tcpClient = connectToMasterAsync host port |> Async.RunSynchronously
    logProvider.AddLogger(NetworkLoggerSlave(logLevel, tcpClient, sink)))
  |> ignore

  (results, logProvider, logSource)
