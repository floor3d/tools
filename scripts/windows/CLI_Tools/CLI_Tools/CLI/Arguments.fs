﻿module Bootstrap.CLI.Arguments

open System.Diagnostics.CodeAnalysis
open Argu
open ES.Fslog
open Bootstrap.Servers.ServerStratum

/// CLI Arguments. These are what the user provides, and are parsed into a
/// 'RunConfig' for actual program execution.
[<DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.All)>]
type Arguments =
  /// Where we should send logging information to. That is, who is listening to
  /// our log entries?
  | [<Unique; AltCommandLine("-l")>] Listener of host: string * port: int
  /// The filepath to save logging information to.
  | [<AltCommandLine("-o")>] OutFile of filepath: string
  /// How verbose logging should be.
  | [<Unique; AltCommandLine("-v")>] Verbosity of level: LogLevel
  | [<Unique; AltCommandLine("-s")>] Stratum of stratum: ServerStratum
  | [<Unique>] SkipProp
  | [<Unique>] SignRPCs of doSigning: bool // TODO: implement MAC checks
  | [<Unique>] MasterPublicKey of b64RSAParameters: string // TODO: implement b64 -> RSAParameters deserialization

  interface IArgParserTemplate with
    member this.Usage =
      match this with
      | Listener _ ->
        "Where logging info should be sent to. Should be either
        an IPv4 address, NETBIOS hostname (for Windows hosts), or DNS address."
      | OutFile _ -> "Where a file of logging info should be saved."
      | Verbosity _ ->
        "Verbosity level of logging. Allowable values: Critical, Error, Warning,
        Informational, Verbose"
      | Stratum _ ->
        "The strata this server belongs to. Usually, there will be one Master
        and many Slaves (and this is the only supported configuration currently)"
      | SkipProp ->
        "Skip the propagation sequence. This is used chiefly for debugging, as the
        use of pseexec (or other means) may take a while."
      | SignRPCs _ ->
        "(UNIMPLEMENTED) Whether to use Public Key Encryption to generate a MAC for Remote
        Procedure Calls (RPCs) send from the Master to a Slave. Defaults to True.
        It is highly recommended that this not be touched. This has no effect
        when used on a Slave"
      | MasterPublicKey _ ->
        "(UNIMPLEMENTED) The Base64 encoded RSAParameters object, which must contain the public
        parameters for the signature verification."
