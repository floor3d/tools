# Path to Sysinternals
$SysinternalsPath = "C:\sysinternals"

# install sysinternals
$installSysinternals = Start-Process powershell.exe -ArgumentList "-File .\install-sysinternals.ps1"

# Run the rest

# windows defender script
Start-Process powershell.exe -ArgumentList "-File .\big-harden.ps1"

# see whatz goin on
Start-Process powershell.exe -ArgumentList "-File .\information.bat"

# run all scripts in hardening_scripts
$directoryPath = ".\hardening_scripts"

$scriptFiles = Get-ChildItem -Path $directoryPath -Filter *.ps1

foreach ($script in $scriptFiles) {
    Write-Host "Running $($script.Name)..."
    Start-Process powershell.exe -ArgumentList "-File $script.FullName"
}

$installSysinternals.WaitForExit()
# Open Sysinternals tools
Start-Process -FilePath "$SysinternalsPath\tcpview64.exe"
Start-Process -FilePath "$SysinternalsPath\autoruns64.exe"
Start-Process -FilePath "$SysinternalsPath\procexp64.exe"